#ifndef MAP_H
#define MAP_H

#include <vector>

#include "sdlinclude.h"

#include "Tileset.h"

namespace botor
{

class Tile;
class Robot;
class Object;

class Map
{

	Tileset tileset;

	public:
	
	unsigned static const int MAP_WIDTH = 40;
	unsigned static const int MAP_HEIGHT = 28;
	
	unsigned static const int MAP_SHIFT = 2 * Tileset::TILE_HEIGHT;

	private:

	std::vector<std::vector<Tile> > tiles; // ( MAP_WIDTH, std::vector<Tile>( MAP_HEIGHT ) );
	std::vector<Robot*> robots;
	std::vector<Object*> objects;

	
	public:
	
	Map();
	
	bool load( const char *fmap );
	void unload();
	
	void Initialize();

	void Update();
	void Draw( );

	Tile *tileAt( Uint8 mapX, Uint8 mapY );
	
	bool PushPushable( Uint8 fromX, Uint8 fromY, Uint8 dirX, Uint8 dirY );
	
	void RemoveTile( Uint8 mapX, Uint8 mapY );
	
	bool isRobotOn( Uint8 mapX, Uint8 mapY );
	
	void Acid( Uint8 cX, Uint8 cY );
	
	void Explode( Uint8 X, Uint8 Y );
	
	bool checkFree( Uint8 X, Uint8 Y );

	Object *getObject( Uint8 X, Uint8 Y );
	void insertObject( Object *o, Uint8 X, Uint8 Y );
	void insertRobot( Uint8 X, Uint8 Y );

	void getRandomFreeTile( Uint8 &X, Uint8 &Y );

	static void transformM2S( Uint8 mapX, Uint8 mapY, Sint16 &x, Sint16 &y ); //MapCoordinates to ScreenC.
	
	

};

}
#endif
