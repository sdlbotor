#include "globalfunc.h"

#include "Player.h"
#include "Game.h"
#include "Tile.h"
#include "Object.h"
#include "Map.h"

namespace botor
{

	Tileset Player::PLAYER_TILESET( "player.png" );

	Player::Player( Uint8 X, Uint8 Y ) : LivingMapObject( X, Y ), lives(3), 
		ani_idle( 0, 0, 0, false ),
		ani_walking( 1, 2, 10 ),
		ani_immuneIdle( 3, 0, 0, false ),
		ani_immuneWalking( 4, 2, 10 ),
		immune(false)
	{
	
		ani_player = new DAnimation( &PLAYER_TILESET );
		
		p_ani_idle = &ani_idle;
		p_ani_walking = &ani_walking;
		
 		graphic = ani_player;
		ani_player->load( p_ani_idle );

	}
	
// 	Player::~Player() { }

	void Player::InitWalking( Sint8 X, Sint8 Y )
	{
		if( !walking )
		{
			vX = X;
			vY = Y;
			walking = true;
			ani_player->load( p_ani_walking );
			StartMovement();
		}
	}


	float Player::SPEED()
	{
		return PLAYER_SPEED;
	}	

	bool Player::isWalkable( Uint8 X, Uint8 Y )
	{
		Tile *t = Game::getMap()->tileAt(X, Y);
		
		if( isBitSet( t->typeMask, Tile::TILE_EFENCE ) )
		{
			if( immune )
			{
				if( Game::getMap()->PushPushable( X, Y, vX, vY ) )
				{
					return true;
				}
			}
			else
				return true;
		}
		else if( isBitSet( t->typeMask, Tile::TILE_WALKABLE ) )
		{
			return true;
		}
		else if( isBitSet( t->typeMask, Tile::TILE_PUSHABLE ) )
		{
			if( Game::getMap()->PushPushable( X, Y, vX, vY ) )
			{
				return true;
			}
		}
		
		return false;
	}
	
	void Player::OnTile( Tile *t )
	{
		if( isBitSet( t->typeMask, Tile::TILE_EFENCE ) )
		{
			Game::getMap()->RemoveTile( mapX, mapY );
			Die();
		}
		
		while( Object *o = Game::getMap()->getObject( mapX, mapY ) )
		{
			o->OnPlayerOnTile( this );
		}
	}
	
	void Player::OnDead()
	{
		lives--;
	}
	
	void Player::HandleInput( SDL_Event *event )
	{
		switch( event->type )
		{

				case SDL_KEYDOWN:


					switch( event->key.keysym.sym )
					{
						case SDLK_DOWN:
							InitWalking( 0, +1 );
							break;
						case SDLK_UP:
							InitWalking( 0, -1 );
							break;
						case SDLK_LEFT:
							InitWalking( -1, 0 );
							break;
						case SDLK_RIGHT:
							InitWalking( +1, 0 );
							break;
						case SDLK_RETURN:
							if( !isAlive() )
								ResetPlayer();
							else
								Use( 0 );
							break;
						case SDLK_d:
							Drop( 0 );
							break;
						//case SDLK_a:
						//	Game::getMap()->Acid( mapX, mapY );
						//	break;
						default:
							break;


				}
				break;
					
					
			
			case SDL_KEYUP:
				switch( event->key.keysym.sym )
				{
					case SDLK_DOWN:
					case SDLK_UP:
						vY = 0;
						walking = false;
						ani_player->load( p_ani_idle );
						break;
					case SDLK_LEFT:
					case SDLK_RIGHT:
						vX = 0;
						ani_player->load( p_ani_idle );
						walking = false;
						break;
					default:
						break;
				}
		}
		
	}
	
	void Player::Draw()
	{
		
		inv.Draw( 0,0 );
		
		LivingMapObject::Draw();
	}
	
	void Player::ResetPlayer()
	{
		Teleport( 1, 1 );
		Reanimate();
	}
	
	unsigned int Player::getLives() { return lives; }

	void Player::Pickup( Object *o )
	{
		o->OnPickup( this );
		inv.addObject( o );
	}
	
	void Player::Drop( Object *o )
	{
		if( Object *obj = inv.removeObject() )
		{
			obj->OnDrop( this );
			Game::getMap()->insertObject( obj, mapX, mapY );
		}
	}
	
	
	void Player::Use( Object *o )
	{
		if( Object *obj = inv.removeObject() )
		{
		
			obj->OnActivation( this );
		
			if( obj->getNUse() == ObjectClass::NUseOnce )
			{
				delete obj;
			}
			else if( obj->getNUse() == ObjectClass::NUseDrop )
			{
				obj->OnDrop( this );
				Game::getMap()->insertObject( obj, mapX, mapY );
			}
			else
			{
				inv.addObject( obj );
			}
		}
	}
	
		
	void Player::SetImmune( bool newstate )
	{
		immune = newstate;
		
		if( immune )
		{
			p_ani_idle = &ani_immuneIdle;
			p_ani_walking = &ani_immuneWalking;
		}
		else
		{
			p_ani_idle = &ani_idle;
			p_ani_walking = &ani_walking;
		}
	}
	
	
	void Player::addLives( int i  )
	{
		lives += i;
	}

}
