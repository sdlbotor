#include <iostream>

#include "globalfunc.h"

#include "Map.h"
#include "Game.h"
#include "Tile.h"
#include "Player.h"
#include "Robot.h"
#include "Object.h"

namespace botor
{

	Map::Map() : tiles( MAP_WIDTH, std::vector<Tile>( MAP_HEIGHT ) ), robots(), objects()
	{}

	void Map::Initialize()
	{
		
	    	lua_State *L = lua_newthread( Game::LUA );
	    	
	    	int s = luaL_dofile( L, "map.lua" );
	    	
	    	if( s )
	    	{
	    		std::cerr << "Error loading Map:" << std::endl
	    			<< lua_tostring( L, -1 ) << std::endl;
	    		exit(1);
	    	}
	    	
	}

	bool Map::load( const char *fmap )
	{
		char tilefile[255];
		FILE *in = fopen( fmap, "r" );
		if( !in )
			return false;
			
		fscanf( in, "%s", (char*)tilefile );
		
		if( !tileset.load( tilefile ) )
			return false;
		
		for( unsigned int y = 0; y < MAP_HEIGHT; y++ )
		{
			for( unsigned int x = 0; x < MAP_WIDTH; x++ )
			{
				Tile temp;
				temp.moving = false;
				temp.acid = 0;
				
				fscanf( in, "%d:%d ", &temp.tileID, &temp.typeMask );
				
				tiles[x][y] = temp;
			}
			fscanf( in, "\n" );
		}
		
		fclose( in );
		
		return true;
	
	}
	
	void Map::unload()
	{
		tileset.unload();
	}
	
	void Map::Update()
	{
		for( unsigned int x = 0; x < MAP_WIDTH; x++ )
		{
			for( unsigned int y = 0; y < MAP_HEIGHT; y++ )
			{
				Tile &t = tiles[x][y];
				if( t.moving )
				{
					
					Sint16 sx,sy;
					Map::transformM2S( x, y, sx, sy );
					if( dst( t.movementX, t.movementY, (float)sx, (float)sy ) < 0.5 )
					{
						t.moving = false;
					}
					else
					{
						float dirX, dirY;
						
						dirX = sign( sx - t.movementX ) * Player::PLAYER_SPEED;
						dirY = sign( sy - t.movementY ) * Player::PLAYER_SPEED;
						
						t.movementX += dirX;
						t.movementY += dirY;
					}
				}
				
				if( t.acid > 0 )
				{
					if( time(0) > t.timer )
					{
						t.acid--;
						if(!t.acid)
							RemoveTile(x,y);
						t.timer = time(0)+1;
					}

				}
			}
		}
		
		for( unsigned int i = 0; i < robots.size(); i++ )
		{
			Robot *r = robots[i];
			
			r->Update();
			
			if( !r->isAlive() )
			{
				robots.erase( robots.begin() + i );
				delete r;
			}
		}
		
		for( unsigned int i = 0; i < objects.size(); i++ )
		{
			Object *o = objects[i];
			
			o->Update();
			
			if( o->getLocation() != Object::LOC_GROUND )
			{
				objects.erase( objects.begin() + i );
			}
		}

	}
	
	void Map::Draw( )
	{
	
		for( unsigned int x = 0; x < MAP_WIDTH; x++ )
		{
			for( unsigned int y = 0; y < MAP_HEIGHT; y++ )
			{
				Tile &t = tiles[x][y];
				
				if( t.tileID == -1 )
					continue;
				
				if( t.moving )
				{
					tileset.Draw( (Sint16)t.movementX, (Sint16)t.movementY, t.tileID );
				}
				else if( t.acid > 0 )
				{
					Sint16 sx,sy;
					Map::transformM2S( (Uint8)x, (Uint8)y, sx, sy );			
					
					tileset.Draw( sx, sy, 5+(4-t.acid) );
						
				}
				else
				{
					Sint16 sx,sy;
					Map::transformM2S( (Uint8)x, (Uint8)y, sx, sy );
					tileset.Draw( sx, sy, tiles[x][y].tileID );
				}
				
			}
		}
		

		for( std::vector<Object*>::iterator o = objects.begin(); o != objects.end(); o++ )
		{
			(*o)->Draw();
		}

		for( std::vector<Robot*>::iterator r = robots.begin(); r != robots.end(); r++ )
		{
			(*r)->Draw();

		}
		
	
	}

	Tile *Map::tileAt( Uint8 mapX, Uint8 mapY )
	{
		return &tiles[mapX][mapY];
	}
	
	bool Map::PushPushable( Uint8 fromX, Uint8 fromY, Uint8 dirX, Uint8 dirY )
	{
		Uint8 toX = fromX+dirX;
		Uint8 toY = fromY+dirY;
		
		Sint16 posX, posY;
		
		if( isBitSet( tiles[toX][toY].typeMask, Tile::TILE_NOTPUSHABLE ) ||
			isBitSet( tiles[toX][toY].typeMask, Tile::TILE_WALL ) ||
			isRobotOn( toX, toY ) ||
			getObject( toX, toY ) )
		{
			return false;
		}
		
		if( isBitSet( tiles[toX][toY].typeMask, Tile::TILE_PUSHABLE ) )
		{
			if(!PushPushable( toX, toY, dirX, dirY ) )
			{
				return false;
			}
		}
		
		Map::transformM2S( fromX, fromY, posX, posY );
		
		tiles[toX][toY] = tiles[fromX][fromY];
		tiles[toX][toY].moving = true;
		tiles[toX][toY].movementX = (float)posX;
		tiles[toX][toY].movementY = (float)posY;	
				
		RemoveTile( fromX, fromY );
		
		return true;
		
	}
	
	void Map::RemoveTile( Uint8 mapX, Uint8 mapY )
	{
		tiles[mapX][mapY].typeMask = Tile::TILE_WALKABLE;
		tiles[mapX][mapY].tileID = -1;
		tiles[mapX][mapY].moving = false;
		tiles[mapX][mapY].acid = 0;
	}
	
	void Map::transformM2S( Uint8 mapX, Uint8 mapY, Sint16 &x, Sint16 &y )
	{
		x = mapX*20;
		y = mapY*20 + MAP_SHIFT;
	}

	bool Map::isRobotOn( Uint8 mapX, Uint8 mapY )
	{
		for( std::vector<Robot*>::iterator r = robots.begin(); r != robots.end(); r++ )
		{
			if( (*r)->getMapX() == mapX && (*r)->getMapY() == mapY )
				return true;
		}
		return false;
	}
	
	void Map::Acid( Uint8 cX, Uint8 cY )
	{
		for( Uint8 y = cY-1; y <= cY+1; y++ )
		{
			for( Uint8 x = cX-1; x <= cX+1; x++ )
			{
				Tile &t = tiles[x][y];
				if( isBitSet( t.typeMask, Tile::TILE_WALL ) && 
					!isBitSet( t.typeMask, Tile::TILE_SOLID) &&
					!t.acid )
				{
					t.acid = 4;
					t.timer = time(0)+1;
				}
			}
		}
	}
	
	Object *Map::getObject( Uint8 X, Uint8 Y )
	{
		for( std::vector<Object*>::iterator o = objects.begin(); o != objects.end(); o++ )
		{
			if((*o)->getLocation() == Object::LOC_GROUND )
			{
				if( (*o)->getMapX() == X && (*o)->getMapY() == Y )
					return *o;
			}
		}
		
		return 0;
	}
	
	void Map::insertObject( Object *o, Uint8 X, Uint8 Y )
	{
		o->setLocation( Object::LOC_GROUND );
		o->Teleport( X, Y );
		
		objects.push_back( o );
	}
	
	void Map::insertRobot( Uint8 X, Uint8 Y )
	{
		robots.push_back( new Robot( X, Y ) );
	}
	
	void Map::getRandomFreeTile( Uint8 &X, Uint8 &Y )
	{
		X = 0;
		Y = 0;
		while( X == 0 && Y == 0 )	//possible deadlock(uh!)
		{
			unsigned int c = 0, cc = rand() % (MAP_HEIGHT*MAP_WIDTH);
			for( unsigned int y = 0; y < MAP_HEIGHT; y++ )
			{
				for( unsigned int x = 0; x < MAP_WIDTH; x++ )
				{
					if( checkFree( x, y ) )
					{
						if( c++ == cc )
						{
							X = x;
							Y = y;
							return;
						}
					}
				}
			}
		}
	}
	
	bool Map::checkFree( Uint8 X, Uint8 Y )
	{
		return isBitSet( tiles[X][Y].typeMask, Tile::TILE_WALKABLE ) &&
						!getObject( X, Y ) &&
						!isRobotOn( X, Y );
	}
	
}

