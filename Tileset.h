#ifndef TILESET_H
#define TILESET_H

#include "sdlinclude.h"

namespace botor
{

//template<unsigned int TILE_WIDTH, unsigned int TILE_HEIGHT>
class Tileset
{
	
	SDL_Surface *tileset;
	
	unsigned int SET_WIDTH;
	unsigned int SET_HEIGHT;
	
	public:

	unsigned static const int TILE_WIDTH = 20;
	unsigned static const int TILE_HEIGHT = 20;

	Tileset();
	Tileset( const char* ftile );
	//~Tileset();
	
	bool load( const char* ftile );
	
	void unload();
	
	void Draw( Sint16 x, Sint16 y, unsigned int tileID );
	
	
};

}

#endif
