#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>

#include "sdlinclude.h"

namespace botor
{

class Object;

struct invent_t
{
	Object *obj;
	unsigned int count;
};

class Inventory
{
	std::vector<invent_t> invent;
	
	typedef std::vector<invent_t>::iterator invElement;
	
	public:
	
	//Inventory();
	
	void addObject( Object *o );
	Object* removeObject( invElement i );
	Object* removeObject( );
	
	bool hasItem( const char *otype );
	
	void Draw( Sint16 X, Sint16 Y );

};

}

#endif
