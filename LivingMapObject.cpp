#include "LivingMapObject.h"

namespace botor
{


	LivingMapObject::LivingMapObject( Uint8 X, Uint8 Y, Drawable *graphic ) :
		MapObject( X, Y, graphic ), alive(true)
	{}

	LivingMapObject::~LivingMapObject() 
	{
// 		delete graphic;
	}

	bool LivingMapObject::isAlive()
	{
		return alive;
	}

	void LivingMapObject::OnDead() {}
	
	void LivingMapObject::Die()
	{
		alive = false;
		OnDead();
	}
	
	void LivingMapObject::Reanimate()
	{
		alive = true;
	}

}
