#ifndef PLAYER_H
#define PLAYER_H

#include "LivingMapObject.h"
#include "DAnimation.h"
#include "Inventory.h"

namespace botor
{

class Object;

class Player : public LivingMapObject
{

	private:
	
	unsigned int lives;
	
	anim_t *p_ani_idle;
	anim_t *p_ani_walking;
	
	anim_t ani_idle;
	anim_t ani_walking;
	
	anim_t ani_immuneIdle;
	anim_t ani_immuneWalking;
	
	DAnimation *ani_player;
	
	Inventory inv;
	
	static Tileset PLAYER_TILESET;

	protected:

	
	bool immune;
	
	public:
	
	static const float PLAYER_SPEED = 0.1f;
	
	public:
	
	Player( Uint8 X = 0, Uint8 Y = 0 );
// 	~Player();
	
	private:

	void InitWalking( Sint8 X, Sint8 Y );
	


	protected:
	
	float SPEED();
	
	bool isWalkable( Uint8 X, Uint8 Y );

	public:
	
	virtual void Draw( );
	
	void HandleInput( SDL_Event *event );
	
	void OnTile( Tile *t );
	void OnDead();
	
	void ResetPlayer();
	
	void Pickup( Object *o );
	void Drop( Object *o );
	void Use( Object *o );
	
	void SetImmune( bool newstate );
	
	unsigned int getLives();
	
	void addLives( int i = 1 );
	
	
};

}

#endif
