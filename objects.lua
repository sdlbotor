objects = { }

objects["acid"] = 
{
	tile = 0,
	nuse = "Once",
}


objects["blueman"] =
{
	tile = 1,
	nuse = "Drop",

}

objects["potion"] =
{
	tile = 2,
	nuse = "Once"
}

objects["hihi"] =
{
	tile = 1,
	nuse = "Once",

}

objects["bomb"] =
{
	tile = 10,
	nuse = "Once",

}


--ACID FUNCTIONS

function objects.acid.Activation( obj )
	pos = player.getPosition();
	map.acid( pos.x, pos.y );
end

--BLUEMAN FUNCTIONS

function objects.blueman.Pickup( obj )
	player.setImmune( 1 );
end

function objects.blueman.Drop( obj )
	player.setImmune( 0 );
end

-- POTION

function objects.potion.Activation( obj )
	player.addLives( );
end

--HIHI FUNCTIONS

function objects.hihi.Activation( obj )
	pos = player.getPosition();
	for i = pos.x+1,pos.x+10,1 do
		map.CreateObject( "acid", i, pos.y )
	end
end

--BOMB

function objects.bomb.Activation( obj )
	pos = player.getPosition();
	for x = pos.x-1,pos.x+1,1 do
		for y = pos.y-1,pos.y+1,1 do
			if not (x == pos.x and y == pos.y) then
				if map.CheckFree( x, y ) then
					map.CreateRobot( x, y )
				end
			end
		end
	end
end

