#include <cmath>
#include "globalfunc.h"

namespace botor
{

double dst( float x1, float y1, float x2, float y2 )
{
	return sqrt( pow( y2-y1, 2 ) + pow( x2-x1, 2 ) );
}
	
bool isBitSet( unsigned int mask, unsigned int bit )
{
	return (mask & bit) == bit;
}

}
