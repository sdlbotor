#ifndef DANIMATION_H
#define DANIMATION_H

#include "Tileset.h"
#include "drawable.h"

namespace botor
{

struct anim_t
{
	unsigned int start;
	unsigned int length;
	unsigned int delay;
	bool loop;
	
	anim_t( unsigned int _start, unsigned int _length, unsigned int _delay, bool _loop = true );
};

class DAnimation : public Drawable
{
	
	Tileset *set;
	anim_t *anim;
	
	unsigned int framescount;
	unsigned int pos;
	
	void Reset();
	
	public:
	
	DAnimation( Tileset *set );
	~DAnimation();
	
	void load( anim_t *anim );
	void Draw( Sint16 X, Sint16 Y );
	bool finished();
};


}


#endif
