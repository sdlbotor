#ifndef ROBOT_H
#define ROBOT_H

#include "LivingMapObject.h"
#include "DAnimation.h"

namespace botor
{

class Robot : public LivingMapObject
{

	anim_t ani_walking;
	DAnimation *ani_player;
	
	static Tileset ROBOTS_TILESET;

	public:

	static const float ROBOT_SPEED = 0.03f;
	
	Robot( Uint8 X = 0, Uint8 Y = 0 );
	
	private:
	
	void MakeNewDir();

	protected:
	
	void StartMovement();

	float SPEED();
	
	bool isWalkable( Uint8 X, Uint8 Y );
	
	public:
	
// 	void Update();
	
	void OnTile( Tile *t );
	
	
};

}
#endif
