#ifndef GAME_H
#define GAME_H

#include <map>
#include <string>
#include <time.h>

#include "luainclude.h"
#include "SDLVideoOut.h"


namespace botor
{


class Map;
class Player;
class ObjectClass;

class Game
{

    static SDLVideoOut *video;
//     static Tileset tileset;
    static Map map;
    static Player player;
    


    bool game_is_running;

    static const int TICKS_PER_SECOND = 50;
    static const int MAX_FRAMESKIP = 10;
    static const int SKIP_FRAMES = (CLOCKS_PER_SEC/1000) / TICKS_PER_SECOND;


    public:

    static lua_State *LUA;
    
    static std::map<std::string,ObjectClass> objectTypes;

    Game();
    ~Game();

	private:

    void Initialize();
    void DeInitialize();
    
    void LoadContent();
    int LoadObjects();

    void Update();
    void HandleInput( SDL_Event *event );
    void Draw();

public:

    void run();
    
    static SDLVideoOut *getVideo();
//     static Tileset *getTileset();
    static Map *getMap();
    static Player *getPlayer();
    


};


}

#endif
