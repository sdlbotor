VERSION	= 3.81
CC	= /usr/bin/g++
CFLAGS	= $(shell pkg-config sdl lua5.1 --cflags) -Wall -g
LDFLAGS	= $(shell pkg-config sdl lua5.1 --libs) -lSDL_gfx -lSDL_image

OBJ	= globalfunc.o SDLVideoOut.o Game.o main.o Tileset.o Map.o MapObject.o LivingMapObject.o drawable.o DTile.o DAnimation.o Player.o Robot.o Object.o Inventory.o luafuncs.o


TARGET	= botor

all:	$(TARGET)

$(TARGET): $(OBJ)
		$(CC) -g -o $(TARGET) $(OBJ) $(LDFLAGS)

%.o: %.cpp
	$(CC) -c $(CFLAGS) $<


clean:
	-rm $(TARGET) *~ *.o
