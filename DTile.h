#ifndef DTILE_H
#define DTILE_H

#include "Tileset.h"
#include "drawable.h"

namespace botor
{

class DTile : public Drawable
{
	Tileset *set;
	unsigned int T;

	
	public:
	
	DTile( Tileset *set, unsigned int T = 0 );
	~DTile();
	
	void setTile( unsigned int T );
	void Draw( Sint16 X, Sint16 Y );
};

}

#endif
