#include "Tileset.h"
#include "Game.h"

namespace botor
{
	
	Tileset::Tileset() {}
	Tileset::Tileset( const char* ftile )
	{
		load( ftile );
	}
	
	//template<unsigned int TILE_WIDTH, unsigned int TILE_HEIGHT>
	void Tileset::unload()
	{
		if( tileset ) 
			SDL_FreeSurface( tileset );
	}
	
	//template<unsigned int TILE_WIDTH, unsigned int TILE_HEIGHT>
	bool Tileset::load( const char* ftile )
	{
		if( tileset )
			SDL_FreeSurface( tileset );
		
		tileset = IMG_Load( ftile );
		
		if( tileset )
		{
			SET_WIDTH = tileset->w / TILE_WIDTH;
			SET_HEIGHT = tileset->h / TILE_HEIGHT;
			
			return true;
		}
		else
			return false;
	}
	
	//template<unsigned int TILE_WIDTH, unsigned int TILE_HEIGHT>
	void Tileset::Draw( Sint16 x, Sint16 y, unsigned int tileID )
	{
		SDL_Rect tileRect;

		if(!tileset)
			return;	//throw error?
			
		
		tileRect.x = (tileID % SET_WIDTH) * TILE_WIDTH;
		tileRect.y = ((int)(tileID / SET_WIDTH)) * TILE_HEIGHT;
		tileRect.w = TILE_WIDTH;
		tileRect.h = TILE_HEIGHT;
		
		Game::getVideo()->Blit( tileset, &tileRect, x, y );
	}

}
