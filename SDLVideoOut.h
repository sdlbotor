#ifndef SDLVIDEOOUT_H
#define SDLVIDEOOUT_H

#include "sdlinclude.h"

namespace botor
{

class SDLVideoOut
{

    SDL_Surface *screen;

    public:

    SDLVideoOut( int width, int height, int bpp, Uint32 flags = SDL_HWSURFACE | SDL_DOUBLEBUF );

    Uint32 MapRGBA( Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255 );
    Uint32 MapRGB( Uint8 r, Uint8 g, Uint8 b );

    void Clear( Uint32 color = 0 );

    void Flip( );

    void Blit( SDL_Surface *src, Sint16 x, Sint16 y );
    void Blit( SDL_Surface *src, SDL_Rect *srcRect, Sint16 x, Sint16 y );

    void DrawString( const char *string, Sint16 x, Sint16 y, Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255 );

    int GetWidth();
    int GetHeight();
    
    void Screenshot( const char *file );

};

}
#endif
