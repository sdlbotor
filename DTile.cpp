#include "DTile.h"

namespace botor
{

	DTile::DTile( Tileset *set, unsigned int T )
	{
		this->set = set;
		this->T = T;
	}

	DTile::~DTile() {}
	
	void DTile::setTile( unsigned int T )
	{
		this->T = T;
	}

	void DTile::Draw( Sint16 X, Sint16 Y )
	{
		set->Draw( X, Y, T );
	}

}
