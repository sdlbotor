#ifndef LUAFUNCS_H
#define LUAFUNCS_H

#include "luainclude.h"

#define LUA_FUNC( name ) int name( lua_State *L )

namespace botor
{

	class LuaFuncs
	{
		
		public:
		
		static void Push( lua_State *L );
		
		//MAP:
		
		static LUA_FUNC( Map_Acid );
		static LUA_FUNC( Map_CreateObject );
		static LUA_FUNC( Map_CreateRobot );
		static LUA_FUNC( Map_GetRandomFreePosition );
		static LUA_FUNC( Map_IsTile );
		static LUA_FUNC( Map_IsObjectOn );
		static LUA_FUNC( Map_IsRobotOn );
		static LUA_FUNC( Map_CheckFree );
		
		static const luaL_Reg lMapFuncs[];
			
		
		//PLAYER:
		
		static LUA_FUNC( Player_GetPosition );
		static LUA_FUNC( Player_SetImmune );
		static LUA_FUNC( Player_Teleport );
		static LUA_FUNC( Player_Pickup );
		static LUA_FUNC( Player_Drop );
		static LUA_FUNC( Player_AddLives );
		
		static const luaL_Reg lPlayerFuncs[];
	};
	

}


#endif

