#ifndef GLOBALFUNC_H
#define GLOBALFUNC_H

namespace botor
{
	template<typename T>  inline int sign( T a )
	{
		return (a == 0) ? 0 : ( (a<0) ? -1 : 1 );
	}
	
	double dst( float x1, float y1, float x2, float y2 );
	
	bool isBitSet( unsigned int mask, unsigned int bit );
	
	template<unsigned int N>
	struct Bitmask
	{
		static const unsigned int v = 1 << N;
	};
}

#endif
