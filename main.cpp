#include <cstdlib>
#include <cstdio>
#include <time.h>
#include "Game.h"
#include "luafuncs.h"

botor::Game game;

void init()
{
	if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "Unable to init SDL: %s\n", SDL_GetError() );
		exit(1);
	}

	atexit(SDL_Quit);


	SDL_WM_SetCaption( "botor", "SDLBotor" );
	
	srand(time(0));
	
}


int main ( int argc, char** argv )
{

    init();

    game.run();

    return 0;
}
