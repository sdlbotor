#ifndef LIVINGMAPOBJECT_H
#define LIVINGMAPOBJECT_H

#include "MapObject.h"

namespace botor
{

class LivingMapObject : public MapObject
{
	private:

	bool alive;
	
	public:
	
	LivingMapObject( Uint8 X = 0, Uint8 Y = 0, Drawable *graphic = 0 );
	virtual ~LivingMapObject( );
	
	virtual void OnDead();
	
	//virtual void Draw();
	
	bool isAlive();
	
	void Die();
	
	void Reanimate();

};

}

#endif
