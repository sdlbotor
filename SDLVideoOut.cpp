#include <assert.h>
#include "SDLVideoOut.h"

namespace botor
{

    SDLVideoOut::SDLVideoOut( int width, int height, int bpp, Uint32 flags )
    {
        this->screen = SDL_SetVideoMode( width, height, bpp, flags );

        assert(this->screen); //Abort when no screen could be allocated.

    }

    Uint32 SDLVideoOut::MapRGBA( Uint8 r, Uint8 g, Uint8 b, Uint8 a )
    {
        return SDL_MapRGBA( screen->format, r, g, b, a );
    }

    Uint32 SDLVideoOut::MapRGB( Uint8 r, Uint8 g, Uint8 b )
    {
        return SDL_MapRGB( screen->format, r, g, b );
    }

    void SDLVideoOut::Clear( Uint32 color )
    {
        SDL_FillRect( this->screen, 0, color );
    }

    void SDLVideoOut::Flip( )
    {
        SDL_Flip( this->screen );

    }

    void SDLVideoOut::Blit( SDL_Surface *src, Sint16 x, Sint16 y )
    {
        SDL_Rect dstRect;
        dstRect.x = x;
        dstRect.y = y;
        dstRect.w = src->w;
        dstRect.h = src->h;

        SDL_BlitSurface( src, 0, this->screen, &dstRect );
    }

    void SDLVideoOut::Blit( SDL_Surface *src, SDL_Rect *srcRect, Sint16 x, Sint16 y )
    {
        SDL_Rect dstRect;
        dstRect.x = x;
        dstRect.y = y;
        dstRect.w = srcRect->w;
        dstRect.h = srcRect->h;

        SDL_BlitSurface( src, srcRect, this->screen, &dstRect );

    }

    void SDLVideoOut::DrawString( const char *string, Sint16 x, Sint16 y, Uint8 r, Uint8 g, Uint8 b, Uint8 a )
    {
        //stringColor( this->screen, x, y, string, color );
        stringRGBA( this->screen, x, y, string, r,g,b,a );
    }


    int SDLVideoOut::GetWidth()
    {
        return screen->w;
    }

    int SDLVideoOut::GetHeight()
    {
        return screen->h;
    }
    
    void SDLVideoOut::Screenshot( const char *file )
    {
    	SDL_SaveBMP( screen, file );
    }

}
