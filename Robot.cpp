#include <cstdio>
#include <cstdlib>

#include "globalfunc.h"

#include "Robot.h"
#include "Map.h"
#include "Game.h"
#include "DTile.h"
#include "Player.h"


namespace botor
{

	Tileset Robot::ROBOTS_TILESET( "robots.png" );
	
	Robot::Robot( Uint8 X, Uint8 Y ) : LivingMapObject( X, Y ),
		ani_walking( 0, 3, 10 )
	{ 
		ani_player = new DAnimation( &ROBOTS_TILESET );
		graphic = ani_player;
		ani_player->load( &ani_walking );
		
		walking = true;
		
		
		MakeNewDir();
		StartMovement();
	}

	float Robot::SPEED() { return ROBOT_SPEED; }

        void Robot::StartMovement()
        {
		Sint16 sx,sy;
 		Map::transformM2S( mapX, mapY, sx, sy );
 		movementX = (float)sx;
 		movementY = (float)sy;
		
		if( !isWalkable( mapX+vX, mapY+vY ) )
			MakeNewDir();
		
		mapX += vX;
		mapY += vY;
		if( isBitSet( Game::getMap()->tileAt( mapX, mapY )->typeMask, Tile::TILE_SLOW ) )
			speed = SPEED() * 0.5f;
		else
			speed = SPEED();
			
		moving = true;

        }

	bool Robot::isWalkable( Uint8 X, Uint8 Y )
	{
		Tile *t = Game::getMap()->tileAt(X, Y);
		if( 
			(isBitSet( t->typeMask, Tile::TILE_WALKABLE ) || 
			isBitSet( t->typeMask, Tile::TILE_EFENCE )) && 
			!Game::getMap()->isRobotOn(X, Y)  &&
			!Game::getMap()->getObject( X, Y )
			)
		{
			if( isBitSet( t->typeMask, Tile::TILE_EFENCE ) )
			{
				return rand()%4 == 0;
					
			}
				return true;
		}
		else
			return false;
	}
		
	void Robot::OnTile( Tile *t )
	{
		if( isBitSet( t->typeMask, Tile::TILE_EFENCE ) )
		{
			Game::getMap()->RemoveTile( mapX, mapY );
			Die();
		}
		
		if( mapX == Game::getPlayer()->getMapX() &&
			mapY == Game::getPlayer()->getMapY() )
		{
			Game::getPlayer()->Die();
			
			if( rand()%4 == 0 )
				Die();
		}
		
		MakeNewDir();
	}
	
	void Robot::MakeNewDir()
	{
		vX = sign( Game::getPlayer()->getMapX() - mapX );
		vY = sign( Game::getPlayer()->getMapY() - mapY );
		
		if( !isWalkable( mapX+vX, mapY+vY ) )
		{
			if( isWalkable( mapX, mapY+vY ) )
				vX = 0;
			else if( isWalkable( mapX+vX, mapY ) )
				vY = 0;
			else
			{
		
				for( Uint8 y = mapY-1; y <= mapY+1; y++ )
				{
					for( Uint8 x = mapX-1; x <= mapX+1; x++ )
					{
						if( isWalkable( x, y ) )
						{
							vX = sign( x - mapX );
							vY = sign( y - mapY );
							return;
						}
					}
				}
				Die();
			}
		}
		
	}

}
