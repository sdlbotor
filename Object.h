#ifndef OBJECT_H
#define OBJECT_H

#include <string>
#include "MapObject.h"
#include "Tileset.h"
#include "luainclude.h"
//#include "Player.h"

namespace botor
{

class Player;

struct ObjectClass
{

	std::string id;

	enum Graphic
	{
		GRA_TILE,
		GRA_ANIMATION
	} graphic;
	
	int tile_id;
	int anim_length;
	int anim_time;
	bool anim_loop;

	enum NUse
	{
		NUseOnce,
		NUseMulti,
		NUseDrop
	} nUse;
	
	int OnActivation;
	int OnPlayerOnTile;
	int OnPickup;
	int OnDrop;
	
	ObjectClass()
	{
		OnActivation = LUA_REFNIL;
		OnPlayerOnTile = LUA_REFNIL;
		OnPickup = LUA_REFNIL;
		OnDrop = LUA_REFNIL;
	}

};

class Object : public MapObject
{
	protected:

	static Tileset OBJECTS_TILESET;

	public:
	
	enum OLocation
	{
		LOC_FREE,
		LOC_GROUND,
		LOC_INVENT
	};
	
	
	private:
	
	ObjectClass *oclass;
	
	//ObjectClass::NUse nUse;
	OLocation oLoc;

	public:
	
	Object( ObjectClass *CLASS, Sint16 X, Sint16 Y );
	Object( ObjectClass *CLASS );
	
	virtual ~Object( );
	
	private:
		
	void Init( ObjectClass *CLASS );
	
	protected:
	
	virtual float SPEED() { return 0; }
	
	public:
	
	void OnActivation( Player *p );
	void OnPlayerOnTile( Player *p );
	
	void OnPickup( Player *p );
	void OnDrop( Player *p );
	
	OLocation getLocation( ) { return oLoc; }
	void setLocation( OLocation loc ) { oLoc = loc; }
	
	ObjectClass::NUse getNUse() { return oclass->nUse; }
	
	bool checkID( std::string id );

	std::string &getID( );

};


}

#endif
