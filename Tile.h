#ifndef TILE_H
#define TILE_H

#include <time.h>

#include "globalfunc.h"

namespace botor
{

struct Tile
{


	static const unsigned int TILE_NONE = 0,
		TILE_WALL	=	Bitmask<0>::v,
		TILE_WALKABLE	=	Bitmask<1>::v,
		TILE_PUSHABLE 	=	Bitmask<2>::v,
		TILE_EFENCE	=	Bitmask<3>::v,
		TILE_SLOW	=	Bitmask<4>::v,
		TILE_NOTPUSHABLE=	Bitmask<5>::v,
		TILE_SOLID	=	Bitmask<6>::v;
		

    
    unsigned int typeMask;
    int tileID;
    
    bool moving;
    
    unsigned int acid;
    
    time_t timer;
    
    float movementX,movementY;


};

}
#endif

