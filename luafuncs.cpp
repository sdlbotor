#include "luafuncs.h"
#include "globalfunc.h"

#include "Game.h"
#include "Player.h"
#include "Map.h"
#include "Object.h"

template<typename T>
void addcolumn( lua_State *l, const char *name, T value );

template<>
void addcolumn<lua_Number>( lua_State *l, const char *name, lua_Number value )
{
	lua_pushstring( l, name );
	lua_pushnumber( l, value );
	lua_settable( l, -3 );
}

template<>
void addcolumn<const char*>( lua_State *l, const char *name, const char* value )
{
	lua_pushstring( l, name );
	lua_pushstring( l, value );
	lua_settable( l, -3 );
}

int argcheck( lua_State *l, int argc )
{
	int largc = lua_gettop( l );
	if( largc != argc )
		return luaL_error( l, "Got %d arguments, expected %d!", largc, argc );
	return 0;
}

namespace botor
{


	void LuaFuncs::Push( lua_State *L )
	{
		
		luaL_register( L, "map", lMapFuncs );

		luaL_register( L, "player", lPlayerFuncs );
		
	}

	LUA_FUNC( LuaFuncs::Map_Acid )
	{
		int largc = lua_gettop( L );
		if( largc != 2 )
			return luaL_error( L, "Got %d arguments, needed exactly 2!", largc );
		
		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		
		Game::getMap()->Acid( X, Y );
		
		return 0;
	}
	
	LUA_FUNC( LuaFuncs::Map_CreateObject )
	{
		int largc = lua_gettop( L );
		if( largc != 3 )
			return luaL_error( L, "Got %d arguments, needed exactly 3!", largc );
			
		const char *objname = luaL_checkstring( L, 1 );
		Uint8 X = (Uint8)luaL_checknumber( L, 2 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 3 );
		
		Game::getMap()->insertObject( new Object( &Game::objectTypes[objname] ) , X, Y  );
		
		return 0;
	}
	
	LUA_FUNC( LuaFuncs::Map_CreateRobot )
	{
		int largc = lua_gettop( L );
		if( largc != 2 )
			return luaL_error( L, "Got %d arguments, needed exactly 2!", largc );
			
		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		
		Game::getMap()->insertRobot( X, Y  );
		
		return 0;
	}
	
	LUA_FUNC( LuaFuncs::Map_GetRandomFreePosition )
	{
		lua_newtable( L );
		
		Uint8 x,y;
		Game::getMap()->getRandomFreeTile( x, y );
		
		addcolumn<lua_Number>( L, "x", x );
		addcolumn<lua_Number>( L, "y", y );
		
		return 1;
	}
	
	LUA_FUNC( LuaFuncs::Map_IsTile )
	{
		int largc = lua_gettop( L );
		if( largc != 3 )
			return luaL_error( L, "Got %d arguments, needed exactly 3!", largc );
		
		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		unsigned int mask = (Uint8)luaL_checknumber( L, 3 );
		
		lua_pushboolean( L, (int)isBitSet( Game::getMap()->tileAt( X, Y )->typeMask, mask ) );
		
		return 1;
	}
	
	LUA_FUNC( LuaFuncs::Map_IsObjectOn )
	{
		int largc = lua_gettop( L );
		if( largc != 2 )
			return luaL_error( L, "Got %d arguments, needed exactly 2!", largc );
			
		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		
		lua_pushboolean( L, (int)!!Game::getMap()->getObject( X, Y ) );
		
		return 1;
	}

	LUA_FUNC( LuaFuncs::Map_IsRobotOn )
	{
		int largc = lua_gettop( L );
		if( largc != 2 )
			return luaL_error( L, "Got %d arguments, needed exactly 2!", largc );

		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		
		lua_pushboolean( L, (int)Game::getMap()->isRobotOn( X, Y ) );
		
		return 1;
	}
	
	LUA_FUNC( LuaFuncs::Map_CheckFree )
	{
		argcheck( L, 2 );
		
		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		
		lua_pushboolean( L, (int)Game::getMap()->checkFree( X, Y ) );
		
		return 1;
	}

	LUA_FUNC( LuaFuncs::Player_GetPosition )
	{
		Player *p = Game::getPlayer();
		
		lua_newtable( L );
		
		addcolumn<lua_Number>( L, "x", p->getMapX() );
		addcolumn<lua_Number>( L, "y", p->getMapY() );
				
		return 1;
	}
	
	LUA_FUNC( LuaFuncs::Player_SetImmune )
	{
		int largc = lua_gettop( L );
		if( largc != 1 )
			return luaL_error( L, "Got %d arguments, needed exactly 1!", largc );
		
		bool newstate = (bool)luaL_checknumber( L, 1 );
		
		Game::getPlayer()->SetImmune( newstate );
		
		return 0;
	}
	
	LUA_FUNC( LuaFuncs::Player_Teleport )
	{
		int largc = lua_gettop( L );
		if( largc != 2 )
			return luaL_error( L, "Got %d arguments, needed exactly 2!", largc );
		
		Uint8 X = (Uint8)luaL_checknumber( L, 1 );
		Uint8 Y = (Uint8)luaL_checknumber( L, 2 );
		
		Game::getPlayer()->Teleport( X, Y );
		
		return 0;
	}
	
	LUA_FUNC( LuaFuncs::Player_AddLives )
	{
		int largc = lua_gettop( L );
		if( largc == 0 )
		{
			Game::getPlayer()->addLives( );
		}
		else if( largc == 1 )
		{
			Game::getPlayer()->addLives( (int)luaL_checkinteger( L, 1 ) );
		}
		else
			return luaL_error( L, "Got %d arguments, expected 0 or 1!", largc );
			
		return 0;
		
		
	}
	
	LUA_FUNC( LuaFuncs::Player_Pickup ) 
	{
		argcheck( L, 1 );
		
		Object *o = (Object*)lua_touserdata( L, -1 );
		
		Game::getPlayer()->Pickup( o );
		
		return 0;		
	}
	
	LUA_FUNC( LuaFuncs::Player_Drop ) { return 0; }
	
	const luaL_Reg LuaFuncs::lMapFuncs[] = {
		{"acid", Map_Acid },
		{"CreateObject", Map_CreateObject },
		{"CreateRobot", Map_CreateRobot },
		{"GetRandomFreePos", Map_GetRandomFreePosition },
		{"CheckTile", Map_IsTile },
		{"IsObjectOn", Map_IsObjectOn },
		{"IsRobotOn", Map_IsRobotOn },
		{"CheckFree", Map_CheckFree },
		{NULL, NULL}
	};	
	
	const luaL_Reg LuaFuncs::lPlayerFuncs[] = {
		{ "getPosition", Player_GetPosition },
		{ "setImmune", Player_SetImmune },
		{ "addLives", Player_AddLives },
		{ "teleport", Player_Teleport },
		{ "pickup", Player_Pickup },
		{ "drop", Player_Drop },
		{ NULL, NULL }
	};

}
