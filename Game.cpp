#include <time.h>
#include <iostream>
#include <sstream>
#include <assert.h>

#include "luafuncs.h"

#include "Game.h"
#include "Player.h"
#include "Map.h"
#include "Object.h"

namespace botor
{

	SDLVideoOut *Game::video = 0;
	Map Game::map;
	Player Game::player;
	
	lua_State *Game::LUA = 0;
	
	std::map<std::string, ObjectClass> Game::objectTypes;


    Game::Game()
    {
        video = new SDLVideoOut( 800, 600, 32 );
        
        LUA = lua_open();	//open scripting unit
        
        luaopen_base( LUA );
        
	LuaFuncs::Push( LUA );	//and load libraries

    }

    Game::~Game()
    {
    	lua_close( LUA );
    	
        delete video;
    }

    void Game::Initialize()
    {

	Game::map.load( "map.txt" );
	Game::map.Initialize();
	

    }
    
    void Game::LoadContent()
    {
    	LoadObjects();
    }
    
    int Game::LoadObjects()
    {
    	lua_State *L = lua_newthread( LUA );
    	
    	int s = luaL_dofile( L, "objects.lua" );
    	
    	if( s )
    	{
    		std::cerr << "Error loading Objects:" << std::endl
    			<< lua_tostring( L, -1 ) << std::endl;
    		exit(1);
    	}
    	
    	lua_getglobal( L, "objects" );
    	
    	lua_pushnil( L );

    	while( lua_next( L, -2 ) )	//each object type
    	{
    		
    		if( lua_istable( L, -1 ) )
    		{
    			ObjectClass temp;
    			
    			lua_pushnil( L );
    			
    			while( lua_next( L, -2 ) )
    			{
    				
    				std::string key( lua_tostring( L, -2 ) );
    				
    				if( key == "tile" )
    				{
					temp.graphic = ObjectClass::GRA_TILE;
					temp.tile_id = lua_tointeger( L, -1 );
    				}
    				else if( key == "animation" )
    				{
    					temp.graphic = ObjectClass::GRA_ANIMATION;
    					
    					lua_getfield( L, -1 ,"id" );
    					
    					temp.tile_id = lua_tointeger( L, -1 );
    					
    					lua_pop( L, 1 );
    					
    					lua_getfield( L, -1, "length" );
    					
    					temp.anim_length = lua_tointeger( L, -1 );
    					
    					lua_pop( L, 1 );
    					
    					lua_getfield( L, -1, "time" );
    					
    					temp.anim_time = lua_tointeger( L, -1 );
    					
    					lua_pop( L, 1 );
    					
    					lua_getfield( L, -1, "loop" );
    					
    					temp.anim_loop = (bool)lua_toboolean( L, -1 );
    					
    					lua_pop( L, 1 );
    					
    				}
    				else if( key == "nuse" )
    				{

					if( lua_isstring( L, -1 ) )
					{
						std::string nuse( lua_tostring( L, -1 ) );
						
						if( nuse == "Once" )
							temp.nUse = ObjectClass::NUseOnce;
						else if( nuse == "Multi" )
							temp.nUse = ObjectClass::NUseMulti;
						else if( nuse == "Drop" )
							temp.nUse = ObjectClass::NUseDrop;
					}
					else
						return luaL_error( L, "Not a string!" );
    				
    				}
    				else if( key == "Activation" )
    				{
    					if( lua_isfunction( L, -1 ) )
    					{
    						temp.OnActivation = luaL_ref( L, LUA_REGISTRYINDEX );
    						lua_pushnil( L ); // so the pop doesn't get confused
    					}
    					else
    						return luaL_error( L, "Not a function!" );
    				}
    				else if( key == "PlayerOnTile" )
    				{
    					if( lua_isfunction( L, -1 ) )
    					{
    						temp.OnPlayerOnTile = luaL_ref( L, LUA_REGISTRYINDEX );
    						lua_pushnil( L ); // so the pop doesn't get confused
    					}
    					else
    						return luaL_error( L, "Not a function!" );
    				}
    				else if( key == "Pickup" )
    				{
    					if( lua_isfunction( L, -1 ) )
    					{
    						temp.OnPickup = luaL_ref( L, LUA_REGISTRYINDEX );
    						lua_pushnil( L ); // so the pop doesn't get confused
    					}
    					else
    						return luaL_error( L, "Not a function!" );
    				}
    				else if( key == "Drop" )
    				{
    					if( lua_isfunction( L, -1 ) )
    					{
    						temp.OnDrop = luaL_ref( L, LUA_REGISTRYINDEX );
    						lua_pushnil( L ); // so the pop doesn't get confused
    					}
    					else
    						return luaL_error( L, "Not a function!" );
    				}

				lua_pop( L, 1 );
				
    			}
    			
    			temp.id = lua_tostring( L, -2 );
    			
    			objectTypes[lua_tostring( L, -2 )] = temp;

    		}
    		
    		lua_pop( L, 1 );
    		
    	}
    	
    	return 0;
    	

    }

    void Game::DeInitialize()
    {
	Game::map.unload();
    }

    void Game::Update()
    {
	SDL_Event event;
	
	while( SDL_PollEvent( &event ) )
	{
		HandleInput( &event );
		Game::player.HandleInput( &event );
	}
	
	if( Game::player.isAlive() )
	{
		Game::map.Update();
		Game::player.Update();
	}
	
	if( Game::player.getLives() == 0 )
		game_is_running = false;
	
    
    }

    void Game::HandleInput( SDL_Event *event )
    {
	switch (event->type)
	{
		// exit if the window is closed
		case SDL_QUIT:
		game_is_running = false;
		break;

		// check for keypresses
		case SDL_KEYDOWN:
		{
			// exit if ESCAPE is pressed
			if (event->key.keysym.sym == SDLK_ESCAPE)
				game_is_running = false;
			else if( event->key.keysym.sym == SDLK_F9 )
				Game::video->Screenshot( "screenshot.bmp" );
			
			break;
		}
	} // end switch
    }

    void Game::Draw()
    {
	std::stringstream status;
    
	if( !Game::video )
		return;
		
        Game::video->Clear( Game::video->MapRGB( 255, 255, 255 ) );
	
	Game::map.Draw( );
	
	if(Game::player.isAlive())
		Game::player.Draw( );
	else
		Game::video->DrawString( "You are dead! Press [Enter] to continue!", 0, 0, 255, 0, 0 );
	
	status << "Leben: " << Game::player.getLives() << " Pos: " << (int)Game::player.getMapX() << "/" << (int)Game::player.getMapY();
	
	Game::video->DrawString( status.str().c_str(), 0, 20, 0, 0, 0 );
	
	
        Game::video->Flip();
	
    }

    void Game::run()
    {
        int loops;

	LoadContent();


        Initialize();

        game_is_running = true;
        clock_t nextUpdate = clock();

	while( game_is_running )
        {
            loops = 0;

            while( clock() > nextUpdate && loops < MAX_FRAMESKIP )
            {
                Update();

                nextUpdate += SKIP_FRAMES;

                loops++;
            }

            Draw();
	    
        }

        DeInitialize();

    }

    SDLVideoOut *Game::getVideo()
    {
	return Game::video;
    }
    
    Map *Game::getMap()
    {
	return &Game::map;
    }
    
    Player *Game::getPlayer()
    {
	return &Game::player;
    }

}
