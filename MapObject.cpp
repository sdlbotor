#include "MapObject.h"
#include "Map.h"
#include "Game.h"
#include "Tile.h"


namespace botor
{
        void MapObject::StartMovement()
        {
		Sint16 sx,sy;
 		Map::transformM2S( mapX, mapY, sx, sy );
 		movementX = (float)sx;
 		movementY = (float)sy;
		
                if( isWalkable( mapX+vX, mapY+vY ) )
		{
			mapX +=vX;
			mapY +=vY;
			if( isBitSet( Game::getMap()->tileAt( mapX, mapY )->typeMask, Tile::TILE_SLOW ) )
				speed = SPEED() * 0.5f;
			else
				speed = SPEED();
				
			moving = true;
		}
        }
	
	bool MapObject::isWalkable( Uint8 X, Uint8 Y )
	{
		Tile *t = Game::getMap()->tileAt(X, Y);
		
		if( isBitSet( t->typeMask, Tile::TILE_WALKABLE ) )
                {
			return true;
                }
		else
			return false;
	}

	MapObject::MapObject( Uint8 X, Uint8 Y, Drawable *graphic ) :
		mapX(0), mapY(0), vX(0), vY(0), movementX(0), movementY(0), speed(0), walking(false), moving(false)
	{
		this->graphic = graphic;
		Teleport(X, Y);
	}

	MapObject::~MapObject() 
	{
		delete graphic;
	}	

        void MapObject::UpdatePosition()
        {
                Sint16 sx,sy;
                Map::transformM2S( mapX, mapY, sx, sy );
                if( dst( movementX, movementY, (float)sx, (float)sy ) < 0.5 )
                {
			OnTile( Game::getMap()->tileAt( mapX, mapY ) );

			if( walking )
                                StartMovement();
                        else
                                moving = false;

                }
                else
                {
                        float dirX, dirY;
                        
                        dirX = sign( sx - movementX ) * speed;
                        dirY = sign( sy - movementY ) * speed;
                        
                        movementX += dirX;
                        movementY += dirY;
                        
                }
        }

	void MapObject::Initialize() {}
	void MapObject::DeInitialize() {}

	void MapObject::Update() 
	{
		if( moving )
		{
			UpdatePosition();
		}
	}
	
	void MapObject::Draw() 
	{
		if( !graphic )
			return;
			
		Sint16 sx,sy;
		if( moving )
		{
			sx = (Sint16)movementX;
			sy = (Sint16)movementY;
		}
		else
		{
			Map::transformM2S( mapX, mapY, sx, sy );
		}
		
		graphic->Draw( sx, sy );
	}

	void MapObject::OnTile( Tile *t ) {}


	void MapObject::Teleport( Uint8 mapX, Uint8 mapY, Uint16 room )
	{
		this->mapX = mapX;
		this->mapY = mapY;
		moving = false;
	}

	Uint8 MapObject::getMapX() { return mapX; }
	Uint8 MapObject::getMapY() { return mapY; }
	
	bool MapObject::Walking() { return walking; }
	
	void MapObject::setGraphic( Drawable *graphic )
	{
		this->graphic = graphic;
	}
	
	Drawable *MapObject::getGraphic( )
	{
		return graphic;
	}
	
}	

