#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "sdlinclude.h"

namespace botor
{

struct Drawable
{
	virtual ~Drawable();
	virtual void Draw( Sint16 X, Sint16 Y ) = 0;
};

}

#endif
