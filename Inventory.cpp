//#include <assert.h>
#include "Inventory.h"
#include "Game.h"
#include "Object.h"

namespace botor
{

	//Inventory::Inventory() {}
	
	void Inventory::addObject( Object *o )
	{
//		assert( o->getLocation() == Object::LOC_FREE );
		
		/*if( hasItem( o->getID() ) )
		{
			
		}*/
		
		invent_t temp;
		temp.obj = o;
		temp.count = 1;
		
		o->setLocation( Object::LOC_INVENT );
		
		invent.push_back( temp );
	}
	
	Object* Inventory::removeObject( invElement i )
	{
		Object *o = (*i).obj;
		
		invent.erase( i );
		
		return o;
	}
	
	Object* Inventory::removeObject(  )
	{
		if( !invent.empty() )
			return removeObject( invent.begin() );
		return 0;
	}
	
	void Inventory::Draw( Sint16 X, Sint16 Y )
	{ 
		if(!invent.empty())
		{
			unsigned int c = 0;
			for( invElement i = invent.begin(); i != invent.end(); i++, c++ )
			{
				Object *o = (*i).obj;
			
				o->getGraphic()->Draw( X + (c*20), Y );
			}
		}
	}
	
	/*invent_t *Inventory::hasItem( std::string id )
	{
		if(!invent.empty())
		{
			for( invElement i = invent.begin(); i != invent.end(); i++ )
			{
				if( (*i).obj->checkID( id ) )
					return &(*i);
			}
		}
		return NULL;
	}*/


}
