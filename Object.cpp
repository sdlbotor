#include <iostream>

#include "Object.h"
#include "Game.h"
#include "DTile.h"
#include "DAnimation.h"
#include "Player.h"

namespace botor
{

	Tileset Object::OBJECTS_TILESET( "objects.png" );

	Object::Object( ObjectClass *CLASS, Sint16 X, Sint16 Y ) : MapObject( X, Y )
	{
		Init( CLASS );
		
	}
	
	Object::Object( ObjectClass *CLASS ) : MapObject( 0, 0 ) 
	{ 
		Init( CLASS );
	}
	
	void Object::Init( ObjectClass *CLASS )
	{		
		oclass = CLASS;
		
		oLoc = Object::LOC_GROUND;
		
		switch( oclass->graphic )
		{
			case ObjectClass::GRA_TILE:
				graphic = new DTile( &Object::OBJECTS_TILESET, oclass->tile_id );
				break;
			case ObjectClass::GRA_ANIMATION:
				break;
			default:
				break;
		}
		
	}
	
	Object::~Object() {}
	
	
	void Object::OnActivation( Player *p ) 
	{
		if( oclass->OnActivation != LUA_REFNIL )
		{
			lua_rawgeti( Game::LUA, LUA_REGISTRYINDEX, oclass->OnActivation );
			lua_pushlightuserdata( Game::LUA, this );
			lua_call( Game::LUA, 1, 0 );
		}
	}
	
	void Object::OnPickup( Player *p  ) 
	{
		if( oclass->OnPickup != LUA_REFNIL )
		{
			lua_rawgeti( Game::LUA, LUA_REGISTRYINDEX, oclass->OnPickup );
			lua_pushlightuserdata( Game::LUA, this );
			lua_call( Game::LUA, 1, 0 );
		}
	}
	
	void Object::OnDrop( Player *p ) 
	{
		if( oclass->OnDrop != LUA_REFNIL )
		{
			lua_rawgeti( Game::LUA, LUA_REGISTRYINDEX, oclass->OnDrop );
			lua_pushlightuserdata( Game::LUA, this );
			lua_call( Game::LUA, 1, 0 );
		}
	}
	
	void Object::OnPlayerOnTile( Player *p ) 
	{
		if( oclass->OnPlayerOnTile != LUA_REFNIL )
		{
			lua_rawgeti( Game::LUA, LUA_REGISTRYINDEX, oclass->OnPlayerOnTile );
			lua_pushlightuserdata( Game::LUA, this );
			lua_call( Game::LUA, 1, 0 );
		}
		else	//standard action
		{
			p->Pickup( this );
		}
	}
	
/*	bool Object::checkID( std::string id )
	{
		return oclass->id == id;
	}
	
	std::string & Object::getID( )
	{
		return oclass->id;
	}*/
	
}

