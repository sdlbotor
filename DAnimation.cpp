#include "DAnimation.h"

namespace botor
{
	anim_t::anim_t( unsigned int _start, unsigned int _length, unsigned int _delay, bool _loop )
	{
		start = _start;
		length = _length;
		delay = _delay;
		loop = _loop;
	}

	DAnimation::DAnimation( Tileset *set )
	{
		this->set = set;
		Reset();
	}
	
	DAnimation::~DAnimation() {}
	
	void DAnimation::Reset()
	{
		anim = 0;
		framescount = 0;
		pos = 0;
	}
	
	void DAnimation::load( anim_t *anim )
	{
		if( this->anim == anim )
			return;
		Reset();
		
		this->anim = anim;
	}
	
	void DAnimation::Draw( Sint16 X, Sint16 Y )
	{
		if( anim && set )
		{
			if( anim->length == 0 )
			{
				set->Draw( X, Y, anim->start );
				return;
			}
			
			if( ++framescount >= anim->delay )
			{
				framescount -= anim->delay;
				
				if( anim->loop )
				{
					pos = (pos+1) % (anim->length);
				}
				else
					if( pos < anim->length )
						pos++;
			}
			
			set->Draw( X, Y, anim->start + pos );
		}
	
	}
	
	bool DAnimation::finished()
	{
		if(!anim)		return true;
		if(anim->loop)		return false;
		if(pos<anim->length)	return false;
		
		return true;
	}

}
