#ifndef MAPOBJECT_H
#define MAPOBJECT_H

//#include "sdlinclude.h"

#include "drawable.h"
#include "Tile.h"

namespace botor
{

class MapObject
{

	protected:

	Uint8 mapX, mapY;

	Sint8 vX,vY;
	float movementX, movementY;
	float speed;
	
	bool walking;
	bool moving;

	Drawable *graphic;

	public:

	MapObject( Uint8 mapX = 0, Uint8 mapY = 0, Drawable *graphic = 0 );
	virtual ~MapObject();
	
	protected:
	
	void UpdatePosition();
	virtual void StartMovement();

	virtual float SPEED() = 0;
	
	public:
	
	virtual bool isWalkable( Uint8 X, Uint8 Y );

	virtual void Initialize();
	virtual void DeInitialize();

	virtual void Update();
	virtual void Draw();


	//callbacks
	virtual void OnTile( Tile *t );

	void Teleport( Uint8 mapX, Uint8 mapY, Uint16 room = 0 );

	Uint8 getMapX();	
	Uint8 getMapY();
	
	bool Walking();
	
	void setGraphic( Drawable *graphic );
	Drawable *getGraphic( );

};

}
#endif

